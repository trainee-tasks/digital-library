from django.contrib import admin
from .models import (
  Book,
)

# Register your models here.

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
  '''Book Admin'''
  list_display = ('title', 'author', 'language', 'published_date', 'assigned_user_id')
  list_filter = ('language', 'published_date')