from django import forms
from .models import Book


class BookModelForm(forms.ModelForm):
  assign_to_me = forms.BooleanField(
    required=False,
    initial=True,
    widget=forms.HiddenInput()
  )
  class Meta:
    model = Book
    fields = []