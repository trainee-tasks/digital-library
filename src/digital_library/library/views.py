from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from .models import Book
from .forms import BookModelForm

# Create your views here.
def home(request):
  return render(request, 'library/index.html')

class BookListView(LoginRequiredMixin, ListView):
  template_name = 'library/book_list_detail.html'
  context_object_name = 'books'
  queryset=Book.objects.all()


class UserBookListView(LoginRequiredMixin, ListView):
  template_name = 'library/book_list_detail.html'
  context_object_name = 'books'
  queryset=Book.objects.all()
  
  def get_queryset(self):
    try:
      if self.request.user.is_authenticated:
        obj = Book.objects.filter(assigned_user=self.request.user)
        return obj
    except Exception as e:
      print("LIBRARY_VIEWS_USER_BOOK_LIST_VIEW_GET_QUERYSET_ERROR")
      print(e)
      return None


class BookDetailUpdateView(LoginRequiredMixin, UpdateView):
  template_name = 'library/book_list_detail.html'
  context_object_name = 'book'
  form_class = BookModelForm
  queryset=Book.objects.all()
  
  def form_valid(self, form):
    try:
      if form.cleaned_data['assign_to_me']:
        form.instance.assigned_user = self.request.user
      else:
        form.instance.assigned_user = None
    except Exception as e:
      print("LIBRARY_VIEWS_BOOK_DETAIL_UPDATE_VIEW_FORM_VALID_ERROR")
      print(e)
    return super().form_valid(form)