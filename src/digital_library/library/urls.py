from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .apis import (
  BookAdminApi,
  BookApi
)

router = DefaultRouter()
router.register(prefix='restricted/books', viewset=BookAdminApi, basename='admin-api-book')
router.register(prefix='books', viewset=BookApi, basename='api-book')

api_urlpatterns = router.urls

from .views import (
  home,
  BookListView,
  UserBookListView,
  BookDetailUpdateView,
)

urlpatterns = [
  path('', home, name='home'),
  path('api/', include(api_urlpatterns)),
  path('books/', BookListView.as_view(), name='book-list'),
  path('my-books/', UserBookListView.as_view(), name='user-book-list'),
  path('book/<pk>/', BookDetailUpdateView.as_view(), name='book-detail'),
  # 
]
