from rest_framework.authentication import (
  SessionAuthentication,
  BasicAuthentication
)
from rest_framework.permissions import IsAdminUser
from rest_framework import viewsets
from .models import Book
from .serializers import (
  BookSerializer,
  BookAdminSerializer,
)


class BookAdminApi(viewsets.ModelViewSet):
  '''Book Api View based on Model Viewset (only for Admin use)'''
  queryset = Book.objects.all()
  serializer_class = BookAdminSerializer
  authentication_classes = [BasicAuthentication, SessionAuthentication]
  permission_classes = [IsAdminUser]


class BookApi(viewsets.ReadOnlyModelViewSet):
  '''Book Api View based on Read Only Model Viewset (for Public use)'''
  queryset = Book.objects.all()
  serializer_class = BookSerializer


