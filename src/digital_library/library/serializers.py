from rest_framework import serializers
from .models import Book

class BookSerializer(serializers.HyperlinkedModelSerializer):
  '''Book Serializer'''
  class Meta:
    model = Book
    # fields = ['id', 'title', 'author', 'language', 'publisher', 'published_date']
    exclude = ['assigned_user', 'updated_at', 'created_at']
    extra_kwargs = {
      'url': {'view_name': 'library:api-book-detail'}
    }

class BookAdminSerializer(serializers.ModelSerializer):
  '''Book Serializer (for admin use)'''
  class Meta:
    model = Book
    fields = '__all__'