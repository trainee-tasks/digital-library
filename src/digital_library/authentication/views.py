from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic.edit import FormView

from django.contrib.auth.views import (
  LoginView,
  LogoutView,
)
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from .forms import CustomUserChangeForm

# Create your views here.

def home(request):
  if request.user.is_authenticated:
    return render(request, 'authentication/index.html')
  return redirect("account:login")


class UserLoginView(LoginView):
  '''Custom Login View'''
  template_name = 'authentication/auth.html'
  fields = '__all__'
  redirect_authenticated_user = True
  
  def get_success_url(self):
    return reverse('library:home')

class UserLogoutView(LogoutView):
  '''Custom Login View'''
  template_name = 'authentication/logged_out.html'
  
  def get_success_url(self):
    return reverse('library:home')


class UserRegisterView(FormView):
  template_name = 'authentication/auth.html'
  form_class = UserCreationForm
  
  def get_success_url(self):
    return reverse('library:home')
  
  def form_valid(self, form):
    try:
      print(form.cleaned_data)
      user = form.save()
      if user is not None:
        login(self.request, user)
    except Exception as e:
      print("AUTHENTICATION_VIEWS_CUSTOM_REGISTER_VIEW_FORM_VALID_ERROR")
      print(e)
    return super().form_valid(form)


class UserUpdateView(FormView):
  template_name = 'authentication/index.html'
  form_class = CustomUserChangeForm
  model = get_user_model()
  
  def post(self, request, *args, **kwargs):
    try:
      kwargs['form'] = self.form_class(data=request.POST, instance=self.model.objects.get(pk=request.user.pk))
      if kwargs['form'].is_valid():
        kwargs['form'].save()
        return redirect(self.get_success_url())
    except Exception as e:
      print("AUTHENTICATION_VIEWS_USER_UPDATE_VIEW_POST_ERROR")
      print(e)
    return render(request, self.template_name, context=kwargs)
  
  def get_success_url(self):
    return reverse('account:home')
  
  def form_valid(self, form):
    try:
      print(form.instance)
    except Exception as e:
      print("AUTHENTICATION_VIEWS_CUSTOM_REGISTER_VIEW_FORM_VALID_ERROR")
      print(e)
    return super().form_valid(form)