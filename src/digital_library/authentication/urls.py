from django.urls import path

from .views import (
  home,
  UserLoginView,
  UserRegisterView,
  UserLogoutView,
  UserUpdateView,
)

app_name = 'account'

urlpatterns = [
  path('', home, name='home'),
  path('profile/', UserUpdateView.as_view(), name='update'),
  path('login/', UserLoginView.as_view(), name='login'),
  path('logout/', UserLogoutView.as_view(), name='logout'),
  path('register/', UserRegisterView.as_view(), name='register'),
]
