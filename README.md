# Digital Library

A digital library management system using django.

---

## Installing Dependencies

_Before installing dependencies, you should create a virtual environment._  
Now, install all dependencies using pip and run the below command.

```
pip install -r .\src\requirements.txt
```

**NOTE** - **requirements.txt** file is in the **root** directory **src** folder.

---

## Config files

Before moving on, first create configuration files in the **src** directory -  
`.env` file

```
DEBUG=True
SECRET_KEY=<your_django_project_secret_key>
```

`mysql.cnf` file

```
[client]
database = <mysql_database_name>
user = <mysql_username>
password = <mysql_user_password>
default-character-set = utf8
```

**NOTE** - _Don't forget to replace the variables in both files._

---

## To run the server

1. First move to project folder from root directory in cli using below command.
   ```
   cd .\src\digital_library
   ```
2. Then, you’ll need to do **makemigrations** using the below command.
   ```
   python manage.py makemigrations
   ```
3. Next, you’ll need to **migrate** to the database using below command.
   ```
   python manage.py migrate
   ```
4. Now, run the server using below command.
   ```
   python manage.py runserver
   ```
